import os
import time
import numpy as np
import pickle
import datetime

from sqlalchemy.orm import sessionmaker

from models import db_connect, create_tables, drop_tables

from models import Stock, EndOfDay, Financials, MarginsOfSales, Growth,\
        CashFlow, BalanceSheet, Liquidity, Efficiency



start_time = time.time()

# change to the working directory
pwd = os.getcwd()
os.chdir(pwd+'/data/initial')

# connect to the database
engine = db_connect()
Session = sessionmaker(bind=engine)
session = Session()
stocks = dict(session.query(Stock.code, Stock).all())

print 'Insert EOD ...'
fd = open('eod_ratios.pkl', 'rb')
updated_dic = pickle.load(fd)
n = 0
for code in updated_dic.keys():
    print code
    fd = open(code+'_10y_f.csv','r')
    lines = fd.read().splitlines()[1:]
    for line in lines:
        data = line.split(',')
        stock = stocks[code]
        instance = EndOfDay(
                price_date=datetime.datetime.strptime(data[0], '%m/%d/%Y'),
                price_open=float(data[1]),
                price_high=float(data[2]),
                price_low=float(data[3]),
                price_close=float(data[4]),
                price_volume=float(data[5]),
                dynamic_pe_ratio=float(data[6]),
        )
        instance.stock = stock

    # Notes:
    # 1. To speed up the code, make a commit every 10 stocks.
    # 2. Can not make a single commit because the code gets stuck.
    # 3. Commit stock by stock is slow:~xxxmin
    n += 1
    if n == 10:
        session.commit()
        n = 0
    else:
        continue
session.commit()

print 'Finish inserting EOD.\n'
seconds = time.time() - start_time
print 'Total: ',  int(seconds/60), 'minutes.'
