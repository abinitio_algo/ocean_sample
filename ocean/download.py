############################
# DESCRIPTION: Download data. e.g. EOD etc
# USAGE: python download.py
# NOTE: The downloading info is saved to a pickle file
# TODO:
# UPDATED: 22/06/2015
#############################
import urllib
import numpy as np
import time
import os
import pickle

def get_eod(code_name_list, period='10y'):
    """
    period: 5d, 1m, 3m, ytd, 1y, 3y, 5y, 10y, max
    """
    print '######################'
    print '# Downloading EOD...'
    print '######################'
    ok_dic = {}
    failed_dic = {}
    empty_dic = {}
    updated_dic = {}
    for i, code_name in enumerate(code_name_list, start=1):
        code = code_name[0]
        name = code_name[1]
        try:
            url = "http://performance.morningstar.com/Performance/stock/exportStockPrice.action?t=XHKG:%s&pd=%s&freq=d&sd=&ed=&pg=0&culture=en-US" %(code, period)
            table = urllib.URLopener()
            table.retrieve(url, code+'_'+period+'.csv')
            ok_dic[code] = name

            # check empty files
            size = os.stat(code+'_'+period+'.csv').st_size
            if size == 0:
                empty_dic[code] = name
            else:
                updated_dic[code] = name
            print 'OK!', code_name
        except:
            failed_dic[code] = name
            print 'FAILED!', code_name

    print 'Summary of EOD:'
    print len(failed_dic), 'out of', len(code_name_list), 'stocks are failed!'
    print len(empty_dic), 'out of', len(code_name_list)-len(failed_dic), \
            'stocks are empty!\n'

    fd = open('eod.pkl', 'wb')
    pickle.dump((ok_dic, failed_dic, empty_dic, updated_dic), fd)
    print 'Saving pickle file!\n'

    print '#############################'
    print '# Finish downloading EOD!'
    print '#############################'

def get_financials(code_name_list):
    """
    default: period = '5y'. note: hard-coded as 5-year is free
    option: 5d, 1m, 3m, ytd, 1y, 3y, 5y, 10y, max
    """
    type_list = ['is', 'bs', 'cf']
    print '#############################'
    print '# Downloading financials... #'
    print '#############################'
    ok_dic = {}
    failed_dic = {}
    empty_dic = {}
    updated_dic = {}
    for i, code_name in enumerate(code_name_list, start=1):
        code = code_name[0]
        name = code_name[1]
        print 'Downloading stock: ', code
        try:
            for t in type_list:
                url = "http://financials.morningstar.com/ajax/ReportProcess4CSV.html?&t=XHKG:%s&region=hkg&culture=en-US&cur=&reportType=%s&period=12&dataType=A&order=asc&columnYear=5&curYearPart=1st5year&rounding=3&view=raw&r=827948&denominatorView=raw&number=3" %(code, t)
                table = urllib.URLopener()
                table.retrieve(url, code+'_'+t+'.csv')
                ok_dic[code] = name
                print 'ok: ', t

                # check empty files
                size = os.stat(code+'_'+t+'.csv').st_size
                if size == 0:
                    empty_dic[code] = name
                else:
                    updated_dic[code] = name
            print 'OK!', code_name
        except:
            failed_dic[code] = name
            print 'FAILED!', code_name

    print 'Summary of Financials:'
    print len(failed_dic), 'out of', len(code_name_list), 'stocks are failed!'
    print len(empty_dic), 'out of', len(code_name_list)-len(failed_dic), \
            'stocks are empty!\n'

    # save all the downloading info. into a pickle file
    fd = open('fiancials.pkl', 'wb')
    pickle.dump((ok_dic, failed_dic, empty_dic, updated_dic), fd)
    print 'Saving pickle file!\n'

    print '###############################'
    print '# Finish ownloading financials!'
    print '###############################'

def get_key_ratios(code_name_list):
    """
    no period argument
    """
    print '#############################'
    print '# Downloading key ratios...'
    print '#############################'
    ok_dic = {}
    failed_dic = {}
    empty_dic = {}
    updated_dic = {}
    for i, code_name in enumerate(code_name_list, start=1):
        code = code_name[0]
        name = code_name[1]
        try:
            url = "http://financials.morningstar.com/ajax/exportKR2CSV.html?&callback=?&t=XHKG:%s&region=hkg&culture=en-US&cur=&order=asc" %(code)
            table = urllib.URLopener()
            table.retrieve(url, code+'_key_ratios.csv')
            ok_dic[code] = name

            # check empty files
            fd = open(code+'_key_ratios.csv', 'r')
            lines = fd.read().splitlines()
            size = os.stat(code+'_key_ratios.csv').st_size
            if size == 0:
                empty_dic[code] = name
            elif len(lines) != 111:
                empty_dic[code] = name
            else:
                updated_dic[code] = name
            print 'OK!', code_name
        except:
            failed_dic[code] = name
            print 'FAILED!', code_name

    print 'Summary of Ratios:'
    print len(failed_dic), 'out of', len(code_name_list), 'stocks are failed!'
    print len(empty_dic), 'out of', len(code_name_list)-len(failed_dic), \
            'stocks are empty!\n'

    # save all the downloading info. into a pickle file
    fd = open('ratios.pkl', 'wb')
    pickle.dump((ok_dic, failed_dic, empty_dic, updated_dic), fd)
    print 'Saving pickle file!\n'

    print '###############################'
    print '# Finishing downloading key ratios!'
    print '###############################'


if __name__ == "__main__":
    # load info. of codes and names from official HKex
    fd = open('data/hkgem.csv', 'r')
    #fd = open('data/stockhk.csv', 'r')
    code_name_list = np.loadtxt(fd, dtype='str', delimiter= ',', skiprows=1, unpack=False)
    print '=========================================================='
    print 'Ready to download',len(code_name_list), 'number of stocks'
    print '=========================================================='
    time.sleep(1); print '3'; time.sleep(1); print '2'
    time.sleep(1); print '1'; time.sleep(1); print 'go'

    ########################
    # cd folder
    ########################
    pwd = os.getcwd()
    os.chdir(pwd+'/data/initial')

    ##########################
    # Run XXX
    ##########################
    get_eod(code_name_list, period='10y')
    get_key_ratios(code_name_list)
    #get_financials(code_name_list)

    print '=========================================================='
    print 'Finish downloading',len(code_name_list), 'number of stocks'
    print '==========================================================\n'
    print 'Good night :)'
