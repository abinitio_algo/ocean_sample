import numpy as np
import time
import datetime
import os
import pickle
import pandas as pd

from ocean.models import db_connect
from sqlalchemy.orm import sessionmaker
from ocean.models import Stock, EndOfDay, Financials, MarginsOfSales, Growth,\
        CashFlow, BalanceSheet, Liquidity, Efficiency

def create_valuation_ratio(code):
    """
    For the defination, refer to:
    https://www.quantopian.com/help/fundamentals#valuation-ratios
    """
    ##################################
    # Query all the necessary varibles
    ##################################
    eod_dict = dict(session.query(EndOfDay.price_date,
                          EndOfDay.price_close,
                          ).join(Stock).filter(Stock.code==code).all())

    financials_list = session.query(Financials.fiscal_year,
                          Financials.book_value_per_share,
                          Financials.operating_cash_flow,
                          Financials.shares,
                          Financials.earning_per_share,
                          Financials.free_cash_flow_per_share,
                          Financials.dividends,
                          Financials.book_value_per_share,
                          ).join(Stock).filter(Stock.code==code).all()

    balance_list = session.query(BalanceSheet.fiscal_year,
                          BalanceSheet.intangibles,
                          BalanceSheet.total_current_assets,
                          BalanceSheet.total_current_liabilities,
                          ).join(Stock).filter(Stock.code==code).all()

    ##########################
    # Create a valuation table
    ##########################
    # Add a trading calendar to remove this stupid thing.
    fiscal_years_12 = [datetime.date(2004,12,31), # missing price data (10y limit)
                    datetime.date(2005,12,30), # missing price data (10y limit)
                    datetime.date(2006,12,29),
                    datetime.date(2007,12,31),
                    datetime.date(2008,12,31),
                    datetime.date(2009,12,31),
                    datetime.date(2010,12,31),
                    datetime.date(2011,12,30),
                    datetime.date(2012,12,31),
                    datetime.date(2013,12,31),
                    datetime.date(2014,12,31)]

    fiscal_years_03 = [datetime.date(2004,03,31), # missing price data (10y limit)
                    datetime.date(2005,03,31), # missing price data (10y limit) 
                    datetime.date(2006,03,31),
                    datetime.date(2007,03,30),
                    datetime.date(2008,03,31),
                    datetime.date(2009,03,31),
                    datetime.date(2010,03,31),
                    datetime.date(2011,03,25),
                    datetime.date(2012,03,30),
                    datetime.date(2013,03,29),
                    datetime.date(2014,03,31)]

    # Decide whether the fiscal year is ended in March or December.
    # Here we assume each company issues its report consistently every year!
    if str(financial_list[0][0])[5:7] == '12':
        fiscal_years = fiscal_years_12
    else:
        fiscal_years = fiscal_years_03

    table = []
    for i, fiscal_year in enumerate(fiscal_years):
        # The 'financials' has data for all the years, even though some are
        # 'NaN'. But some price data are missing. So we set the missing price
        # as 'NaN' to compute the 'valuations'.
        try:
            price = float(eod_dict[fiscal_year])
        except:
            price = float('NaN')

        # Make sure the year ordering is correct.
        fin = financials_list[i]
        bal = balance_list[i]

        # Start the calculations
        book_value_per_share = float(fin[1])
        book_value_yield = book_value_per_share / price

        cash_flow_from_operating = float(fin[2])
        average_diluted_shares_outstanding =float(fin[3])
        cfo_per_share = cash_flow_from_operating / average_diluted_shares_outstanding
        cf_yield = cfo_per_share / price

        diluted_eps = float(fin[4])
        earning_yield = diluted_eps / price

        fcf_per_share = float(fin[5])
        fcf_ratio = price / fcf_per_share
        fcf_yield = fcf_per_share / price

        dividend_per_share = float(fin[6])
        payout_ratio = dividend_per_share / diluted_eps

        pb_ratio = price / float(fin[7])
        pcf_ratio = price / fcf_per_share
        pe_ratio = price / diluted_eps

        intangible_assets = float(bal[1])
        tangible_book_value_per_share = (book_value_per_share * float(fin[3]) - intangible_assets)/float(fin[3])
        #tangible_book_value_per_share = (book_value - intangible_assets)/shares
        #tangible_bv_per_share_3yr_avg
        #tangible_bv_per_share_5yr_avg

        current_assets = float(bal[2])
        current_liabilities = float(bal[3])
        working_capital_per_share = (current_assets - current_liabilities) / float(fin[3])
        #working_capital_per_share_3yr_avg
        #working_capital_per_share_5yr_avg

        #append the row to table
        row = [fiscal_year,
            book_value_yield,
            cfo_per_share,
            cf_yield,
            earning_yield,
            fcf_ratio,
            fcf_yield,
            payout_ratio,
            pb_ratio,
            pcf_ratio,
            pe_ratio,
            tangible_book_value_per_share,
            working_capital_per_share]
        table.append(row)

    np.savetxt(code+'_valuation_ratio.csv', table, delimiter=',', fmt='%s')

if __name__ == '__main__':
    start = time.time()

    engine = db_connect()
    Session = sessionmaker(bind=engine)
    session = Session()

    # change to the work directory
    pwd = os.getcwd()
    os.chdir(pwd+'/data/initial')

    ############################
    # create valuation ratios
    ############################
    print 'Create valuation ratios...'
    # load code and name: updated_dic
    fd = open('eod_ratios.pkl', 'r')
    updated_dic = pickle.load(fd)

    # format
    for code in updated_dic.keys():
        print code
        create_valuation_ratio(code)
    print 'Finish creating valuation ratios!\n'
    final = time.time()
    print final-start, 'seconds in total.\n'
