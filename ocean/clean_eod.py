import numpy as np
import time
import datetime
import os
import pickle
import pandas as pd
import locale
locale.setlocale( locale.LC_ALL, 'en_US.UTF-8' )

from ocean.models import db_connect
from sqlalchemy.orm import sessionmaker
from ocean.models import Stock, EndOfDay, Financials, MarginsOfSales, Growth,\
        CashFlow, BalanceSheet, Liquidity, Efficiency

def format_historical_price(code, period='10y', length=-1):
    """
    what's the meaning of empty volume, not 0 volume?
    TODO:
    padding the time series
    """
    ###############################
    # query the eps as a dictionary
    ###############################
    #eps_dic = dict(
    #        session.query(Financials.fiscal_year, Financials.earning_per_share).join(Stock)
    #        .filter(Stock.code==code).all()
    #)

    fd = open(code+'_'+period+'.csv', 'r')
    table = []
    lines = fd.read().splitlines()

    # skip the first line
    #table.append(lines[0])
    data = lines[1].split(',')
    data.append('pe_ratio')
    table.append(data)

    for line in lines[2:length]:
        # encoding error: replace empty charactor \xef\xbf\xbd  volume with "0"
        line = line.replace('\xef\xbf\xbd', '"0"')

        # handle NaN
        #line = line.replace('NaN', '0')

        # don't split the commas in the quotes for volume by add maxsplit
        # this can not be solved by pandas
        data = line.split(',', 5)

        # remove quotes and convert number locale
        v = data[5].replace('"','')
        v = locale.atoi(v)
        data[5] = v

        ##############
        # add PE ratio
        ##############
        #price_close = data[4]
        #year = datetime.date(int(data[0][-4:])-1,1,1) #XXX use last year's eps
        #earning_per_share = eps_dic[year]
        #pe_ratio = float(price_close)/float(earning_per_share)
        #data.append(str(pe_ratio))

        table.append(data)
    np.savetxt(code+'_'+period+'_f.csv', table, delimiter=',', fmt='%s')

if __name__ == '__main__':
    engine = db_connect()
    Session = sessionmaker(bind=engine)
    session = Session()

    # change to the work directory
    pwd = os.getcwd()
    os.chdir(pwd+'/data/initial')

    ###############
    # clean EOD
    ###############
    print 'Cleaning EOD...'
    # load code and name: updated_dic
    fd = open('eod_ratios.pkl', 'r')
    updated_dic = pickle.load(fd)

    # format
    for code in updated_dic.keys():
        print code
        format_historical_price(code)
    print 'Finish cleaning EOD!\n'
