#########################
# update the database
# Notes:
# 1. If automate this, we need the trading calendar.
#########################

import os
import subprocess
import time
import datetime
import numpy as np
import pickle

from download import get_eod
from clean import format_historical_price

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import db_connect, create_tables
from ocean.models import Stock, EndOfDay, Financials, MarginsOfSales, Growth,\
        CashFlow, BalanceSheet, Liquidity, Efficiency
from insert import insert_stock, insert_end_of_day

start_time = time.time()

print '##########################################'
print '# Hello, daily update historical price ...'
print '##########################################\n'


###################################################
# load info. of codes and names from official HKex
###################################################
fd = open('data/stockhk.csv', 'r')
code_name_list = np.loadtxt(fd, dtype='str', delimiter= ',', skiprows=1, unpack=False)
print 'Stock code loaded!\n'


########################
# cd and mkdir folder
########################
# cd /daily
pwd = os.getcwd()
os.chdir(pwd+'/data/daily')
# make /today
date = subprocess.check_output(["date", "+%Y%m%d"]).replace('\n','')
subprocess.call(['mkdir', date])
# cd /today
os.chdir(date)
print 'In', date, 'folder!\n'


###########
# Download
###########
print 'Downloading',len(code_name_list), 'number of stocks ...'
time.sleep(1)
get_eod(code_name_list, period='5d')
print 'Finish downloading',len(code_name_list), 'number of stocks\n'
time.sleep(1)


########
# Clean
########
print 'Cleaning EOD ...'
# load code and name: updated_dic
fd = open('eod.pkl', 'r')
ok_dic, failed_dic, empty_dic, updated_dic = pickle.load(fd)
# loop over codes
for code in updated_dic.keys():
    # length = 3, meaning last day
    # length = 4, meaning last two day, etc...
    format_historical_price(code, period='5d', length=3)
    #subprocess.call(['rm', target_file]) # !empty files remain!
    print 'OK!', code
print 'Finish cleaning EOD!\n'


#########
# Insert
#########
print 'Inserting EOD ...'
engine = db_connect()
Session = sessionmaker(bind=engine)
session = Session()
stocks = dict(session.query(Stock.code, Stock).all())

# For the moment, we stick to the intial sample size
fd = open('../../initial/eod_ratios.pkl', 'rb') #XXX This needs to be updated
updated_dic = pickle.load(fd)
for code in updated_dic.keys():
    print code
    fd = open(code+'_5d_f.csv','r')
    lines = fd.read().splitlines()[1:]
    for line in lines:
        data = line.split(',')
        stock = stocks[code]
        instance = EndOfDay(
                price_date=datetime.datetime.strptime(data[0], '%m/%d/%Y'),
                price_open=float(data[1]),
                price_high=float(data[2]),
                price_low=float(data[3]),
                price_close=float(data[4]),
                price_volume=float(data[5]),
                dynamic_pe_ratio=float(data[6]),
        )
        instance.stock = stock

session.commit()

print 'Finish inserting EOD.\n'
seconds = time.time() - start_time
print 'Total: ',  int(seconds/60), 'minutes.\n'
print '##########################'
print '# Bye, daily update done!'
print '##########################'
