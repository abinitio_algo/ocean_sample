import os
import time
import numpy as np
import pickle
import datetime

from sqlalchemy.orm import sessionmaker

from models import db_connect, create_tables, drop_tables

from models import Stock, ValuationRatio



start_time = time.time()

# change to the working directory
pwd = os.getcwd()
os.chdir(pwd+'/data/initial')

# connect to the database
engine = db_connect()
Session = sessionmaker(bind=engine)
session = Session()
stocks = dict(session.query(Stock.code, Stock).all())

print 'Insert valuation ratios ...'
fd = open('eod_ratios.pkl', 'rb')
updated_dic = pickle.load(fd)
n = 0
for code in updated_dic.keys():
    print code
    fd = open(code+'_valuation_ratio.csv','r')
    lines = fd.read().splitlines()
    for line in lines:
        data = line.split(',')
        stock = stocks[code]
        instance = ValuationRatio(
                fiscal_year=datetime.datetime.strptime(data[0], '%m/%d/%Y'),
                book_value_yield=float(data[1]),
                cfo_per_share=float(data[2]),
                cf_yield=float(data[3]),
                earning_yield=float(data[4]),
                fcf_ratio=float(data[5]),
                fcf_yield=float(data[6]),
                payout_ratio=float(data[7]),
                pb_ratio=float(data[8]),
                pcf_ratio=float(data[9]),
                pe_ratio=float(data[10]),
                tangible_book_value_per_share=float(data[11]),
                working_capital_per_share=float(data[12])
        )
        instance.stock = stock

    # Notes:
    # 1. To speed up the code, make a commit every 10 stocks.
    # 2. Can not make a single commit because the code gets stuck.
    # 3. Commit stock by stock is slow:~xxxmin
    n += 1
    if n == 10:
        session.commit()
        n = 0
    else:
        continue
session.commit()

print 'Finish inserting valuation ratio.\n'
seconds = time.time() - start_time
print 'Total: ',  int(seconds/60), 'minutes.'
