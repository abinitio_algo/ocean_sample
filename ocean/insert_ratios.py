#############################
# func: insert data to the database
#############################

from sqlalchemy.orm import sessionmaker

from models import db_connect, create_tables, drop_tables

from models import Stock, EndOfDay, Financials, MarginsOfSales, Growth,\
        CashFlow, BalanceSheet, Liquidity, Efficiency

import datetime

def insert_stock(code):
    """
    stocks table
    """
    stock = Stock(code=code)
    return stock

def insert_financials(code):
    """
    historicalprices table
    """
    fd = open(code+'_financials_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.financials.append(Financials(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    revenue=float(data[1]),
                    gross_margin_pct=float(data[2]),
                    operating_income=float(data[3]),
                    operating_margin_pct=float(data[4]),
                    net_income=float(data[5]),
                    earning_per_share=float(data[6]),
                    dividends=float(data[7]),
                    payout_ratio_pct=float(data[8]),
                    shares=float(data[9]),
                    book_value_per_share=float(data[10]),
                    operating_cash_flow=float(data[11]),
                    cap_spending=float(data[12]),
                    free_cash_flow=float(data[13]),
                    free_cash_flow_per_share=float(data[14]),
                    working_capital=float(data[15]),
                    ))

def insert_margin_of_sales(code):
    """
    margin of sales table
    """
    fd = open(code+'_margins_of_sales_pct_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.marginofsales.append(MarginsOfSales(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    revenue_pct = float(data[1]),
                    cogs_pct = float(data[2]),
                    gross_margin_pct = float(data[3]),
                    sga_pct = float(data[4]),
                    rd_pct = float(data[5]),
                    other_pct = float(data[6]),
                    operating_margin_pct = float(data[7]),
                    net_int_inc_other_pct = float(data[8]),
                    ebt_margin_pct = float(data[9]),
                    ))

def insert_growth(code):
    """
    growth table
    """
    fd = open(code+'_growth_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.growth.append(Growth(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    revenue_yoy_pct = float(data[1]),
                    revenue_3ya_pct = float(data[2]),
                    revenue_5ya_pct = float(data[3]),
                    revenue_10ya_pct = float(data[4]),
                    operating_income_yoy_pct = float(data[5]),
                    operating_income_3ya_pct = float(data[6]),
                    operating_income_5ya_pct = float(data[7]),
                    operating_income_10ya_pct = float(data[8]),
                    net_income_yoy_pct = float(data[9]),
                    net_income_3ya_pct = float(data[10]),
                    net_income_5ya_pct = float(data[11]),
                    net_income_10ya_pct = float(data[12]),
                    eps_income_yoy_pct = float(data[13]),
                    eps_income_3ya_pct = float(data[14]),
                    eps_income_5ya_pct = float(data[15]),
                    eps_income_10ya_pct = float(data[16]),
                                    ))

def insert_cash_flow(code):
    """
    cash flow table
    """
    fd = open(code+'_cash_flow_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.cashflow.append(CashFlow(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    operating_cash_flow_growth_pct = float(data[1]),
                    free_cash_flow_growth_yoy_pct = float(data[2]),
                    cat_ex_as_a_pct_of_sale = float(data[3]),
                    free_cash_flow_to_sales_pct = float(data[4]),
                    free_cash_flow_to_net_income = float(data[5]),
                                    ))

def insert_balance_sheet(code):
    """
    balance sheet table
    """
    fd = open(code+'_financial_health_balance_sheet_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.balancesheet.append(BalanceSheet(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    cash_short_term_investments = float(data[1]),
                    accounts_receivable = float(data[2]),
                    inventory = float(data[3]),
                    other_current_assets = float(data[4]),
                    total_current_assets = float(data[5]),
                    net_pp_e = float(data[6]),
                    intangibles = float(data[7]),
                    other_long_term_assets = float(data[8]),
                    total_assets = float(data[9]),
                    account_payable = float(data[10]),
                    short_term_debt = float(data[11]),
                    taxes_payable = float(data[12]),
                    accrued_liabilities = float(data[13]),
                    other_short_term_liabilities = float(data[14]),
                    total_current_liabilities = float(data[15]),
                    long_term_debt = float(data[16]),
                    other_long_term_liabilities = float(data[17]),
                    total_liabilities = float(data[18]),
                    total_stockholders_equity = float(data[19]),
                    total_liabilities_equity = float(data[20]),
                                    ))

def insert_liquidity(code):
    """
    liquidity table
    """
    fd = open(code+'_financial_health_liquidity_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.liquidity.append(Liquidity(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    current_ratio = float(data[1]),
                    quick_ratio = float(data[2]),
                    financial_leverage = float(data[3]),
                    debt_to_equity = float(data[4]),
                                    ))

def insert_efficiency(code):
    """
    efficientcy table
    """
    fd = open(code+'_efficiency_ratios_f.csv','r')
    lines = fd.read().splitlines()[:-1]
    for line in lines:
        data = line.split(',')
        stock.efficiency.append(Efficiency(
                    fiscal_year=datetime.datetime.strptime(data[0], '%Y'),
                    days_sales_outstanding = float(data[1]),
                    days_inventory = float(data[2]),
                    payables_period = float(data[3]),
                    cash_conversion_cycle = float(data[4]),
                    receivables_turnover = float(data[5]),
                    inventory_turnover = float(data[6]),
                    fixed_assets_turnover = float(data[7]),
                    asset_turnover = float(data[8]),
                                    ))



if __name__ == '__main__':
    import os
    import time
    import numpy as np
    import pickle

    start_time = time.time()
    # change to the working directory
    pwd = os.getcwd()
    os.chdir(pwd+'/data/initial')

    # connect to the database
    # create tables
    engine = db_connect()
    drop_tables(engine) # very slow!
    print 'hi'
    create_tables(engine)
    Session = sessionmaker(bind=engine)
    session = Session()
    print 'Tables are defined!\n'
    time.sleep(1)

    # insert ratios
    print 'Inserting ratios ...'
    fd = open('eod_ratios.pkl', 'rb')
    updated_dic = pickle.load(fd)
    n = 0
    # Only store the stock with both historical prices and key_ratios
    for code in updated_dic.keys():
        print code
        stock = insert_stock(code)
        insert_financials(code)
        insert_margin_of_sales(code)
        insert_growth(code)
        insert_cash_flow(code)
        insert_balance_sheet(code)
        insert_liquidity(code)
        insert_efficiency(code)

        # Now add stock instance to Stock table
        session.add(stock)

        # Notes:
        # 1. To speed up the code, make a commit every 10 stocks.
        # 2. Can not make a single commit because the code gets stuck.
        # 3. Commit stock by stock is slow:~xxxmin
        n += 1
        if n == 10:
            session.commit()
            n = 0
        else:
            continue
    session.commit()
    print 'Finish inserting ratios.\n'
    seconds = time.time() - start_time
    print 'Total: ',  int(seconds/60), 'minutes.'
