#############################################
# define the database structure, using ORM in sqlalchemy
#############################################

from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Numeric, String, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from sqlalchemy.engine.url import URL

import settings

Base = declarative_base()

def db_connect():
    """"""
    return create_engine(URL(**settings.DATABASE))

def create_tables(engine):
    """"""
    Base.metadata.create_all(engine)

def drop_tables(engine):
    """"""
    Base.metadata.drop_all(engine)

class Stock(Base):
    """"""
    __tablename__ = 'stocks'

    id = Column(Integer, primary_key=True)
    code = Column(String)


class EndOfDay(Base):
    __tablename__ = 'historicalprices'

    id = Column(Integer, primary_key=True)
    price_date = Column(Date)
    price_open = Column(Numeric)
    price_high = Column(Numeric)
    price_low = Column(Numeric)
    price_close = Column(Numeric)
    price_volume = Column(Numeric)
    dynamic_pe_ratio = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('historicalprices', order_by=id))


class ValuationRatio(Base):
    """
    For the defination, refer to:
    https://www.quantopian.com/help/fundamentals#valuation-ratios

    Note: some ratios are commented due to either the lack of data or already exits.
    """
    __tablename__ = 'valuationratios'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    #book_value_per_share [Financials]
    book_value_yield = Column(Numeric)
    #buy_back_yield = Column(Numeric) []
    #cash_return = Column(Numeric) []
    cfo_per_share = Column(Numeric)
    cf_yield = Column(Numeric)
    #dividend_rate = Column(Numeric)
    #dividend_yield = Column(Numeric)
    earning_yield = Column(Numeric)
    #ev_to_ebitda = Column(Numeric)
    #fcf_per_share = Column(Numeric)
    fcf_ratio = Column(Numeric)
    fcf_yield = Column(Numeric)
    #forward_dividend_yield = Column(Numeric)
    #forward_earning_yield = Column(Numeric)
    #forward_pe_ratio = Column(Numeric)
    payout_ratio = Column(Numeric)
    pb_ratio = Column(Numeric)
    pcf_ratio = Column(Numeric)
    pe_ratio = Column(Numeric)
    #peg_payback = Column(Numeric)
    #peg_ratio = Column(Numeric)
    #ps_ratio = Column(Numeric)
    #sales_per_share = Column(Numeric)
    #sales_yield = Column(Numeric)
    #sustainable_growth_rate = Column(Numeric)
    tangible_book_value_per_share = Column(Numeric)
    #tangible_bv_per_share_3yr_avg = Column(Numeric)
    #tangible_bv_per_share_5yr_avg = Column(Numeric)
    #total_yield = Column(Numeric)
    working_capital_per_share = Column(Numeric)
    #working_capital_per_share3_yr_avg = Column(Numeric)
    #working_capital_per_share5_yr_avg = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('valuationratios', order_by=id))


class Financials(Base):
    __tablename__ = 'financials'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    revenue = Column(Numeric)
    gross_margin_pct = Column(Numeric)
    operating_income = Column(Numeric)
    operating_margin_pct = Column(Numeric)
    net_income = Column(Numeric)
    earning_per_share = Column(Numeric)
    dividends = Column(Numeric)
    payout_ratio_pct = Column(Numeric)
    shares = Column(Numeric)
    book_value_per_share = Column(Numeric)
    operating_cash_flow = Column(Numeric)
    cap_spending = Column(Numeric)
    free_cash_flow = Column(Numeric)
    free_cash_flow_per_share = Column(Numeric)
    working_capital = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('financials', order_by=id))


class MarginsOfSales(Base):
    __tablename__ = 'marginofsales'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    revenue_pct = Column(Numeric)
    cogs_pct = Column(Numeric)
    gross_margin_pct = Column(Numeric)
    sga_pct = Column(Numeric)
    rd_pct = Column(Numeric)
    other_pct = Column(Numeric)
    operating_margin_pct = Column(Numeric)
    net_int_inc_other_pct = Column(Numeric)
    ebt_margin_pct = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('marginofsales', order_by=id))


class Growth(Base):
    __tablename__ = 'growth'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    revenue_yoy_pct = Column(Numeric)
    revenue_3ya_pct = Column(Numeric)
    revenue_5ya_pct = Column(Numeric)
    revenue_10ya_pct = Column(Numeric)
    operating_income_yoy_pct = Column(Numeric)
    operating_income_3ya_pct = Column(Numeric)
    operating_income_5ya_pct = Column(Numeric)
    operating_income_10ya_pct = Column(Numeric)
    net_income_yoy_pct = Column(Numeric)
    net_income_3ya_pct = Column(Numeric)
    net_income_5ya_pct = Column(Numeric)
    net_income_10ya_pct = Column(Numeric)
    eps_income_yoy_pct = Column(Numeric)
    eps_income_3ya_pct = Column(Numeric)
    eps_income_5ya_pct = Column(Numeric)
    eps_income_10ya_pct = Column(Numeric)


    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('growth', order_by=id))


class CashFlow(Base):
    __tablename__ = 'cashflow'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    operating_cash_flow_growth_pct = Column(Numeric)
    free_cash_flow_growth_yoy_pct = Column(Numeric)
    cat_ex_as_a_pct_of_sale = Column(Numeric)
    free_cash_flow_to_sales_pct = Column(Numeric)
    free_cash_flow_to_net_income = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('cashflow', order_by=id))


class BalanceSheet(Base):
    __tablename__ = 'balancesheet'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    cash_short_term_investments = Column(Numeric)
    accounts_receivable = Column(Numeric)
    inventory = Column(Numeric)
    other_current_assets = Column(Numeric)
    total_current_assets = Column(Numeric)
    net_pp_e = Column(Numeric)
    intangibles = Column(Numeric)
    other_long_term_assets = Column(Numeric)
    total_assets = Column(Numeric)
    account_payable = Column(Numeric)
    short_term_debt = Column(Numeric)
    taxes_payable = Column(Numeric)
    accrued_liabilities = Column(Numeric)
    other_short_term_liabilities = Column(Numeric)
    total_current_liabilities = Column(Numeric)
    long_term_debt = Column(Numeric)
    other_long_term_liabilities = Column(Numeric)
    total_liabilities = Column(Numeric)
    total_stockholders_equity = Column(Numeric)
    total_liabilities_equity = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('balancesheet', order_by=id))


class Liquidity(Base):
    __tablename__ = 'liquidity'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    current_ratio = Column(Numeric)
    quick_ratio = Column(Numeric)
    financial_leverage = Column(Numeric)
    debt_to_equity = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('liquidity', order_by=id))


class Efficiency(Base):
    __tablename__ = 'efficiency'

    id = Column(Integer, primary_key=True)
    fiscal_year = Column(Date)
    days_sales_outstanding = Column(Numeric)
    days_inventory = Column(Numeric)
    payables_period = Column(Numeric)
    cash_conversion_cycle = Column(Numeric)
    receivables_turnover = Column(Numeric)
    inventory_turnover = Column(Numeric)
    fixed_assets_turnover = Column(Numeric)
    asset_turnover = Column(Numeric)

    stock_id = Column(Integer, ForeignKey('stocks.id'))
    stock = relationship('Stock', backref=backref('efficiency', order_by=id))
