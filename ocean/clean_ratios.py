import numpy as np
import time
import os
import pickle
import pandas as pd

def split_ratio_file(code, index_i, index_f, filename, skiprows=False):
    fd = open(code+'_key_ratios.csv', 'r')
    lines = fd.read().splitlines()
    # slice the csv file
    table = []
    for line in lines[index_i:index_f]:
        #line = line.replace('2004-12', '2004')
        #line = line.replace('2005-12', '2005')
        #line = line.replace('2006-12', '2006')
        #line = line.replace('2007-12', '2007')
        #line = line.replace('2008-12', '2008')
        #line = line.replace('2009-12', '2009')
        #line = line.replace('2010-12', '2010')
        #line = line.replace('2011-12', '2011')
        #line = line.replace('2012-12', '2012')
        #line = line.replace('2013-12', '2013')
        #line = line.replace('2014-12', '2014')

        #line = line.replace('2004-03', '2004')
        #line = line.replace('2005-03', '2005')
        #line = line.replace('2006-03', '2006')
        #line = line.replace('2007-03', '2007')
        #line = line.replace('2008-03', '2008')
        #line = line.replace('2009-03', '2009')
        #line = line.replace('2010-03', '2010')
        #line = line.replace('2011-03', '2011')
        #line = line.replace('2012-03', '2012')
        #line = line.replace('2013-03', '2013')
        #line = line.replace('2014-03', '2014')

        line = line.replace('TTM', 'Last')
        line = line.replace('Latest Qtr', 'Last')
        if skiprows:
            if 'Revenue' in line:
                continue
            elif 'Net Income' in line:
                continue
            elif 'EPS' in line:
                continue
            elif 'Operating Income' in line:
                continue
            else:
                table.append(line)
        else:
            table.append(line)
    np.savetxt(code+'_'+filename+'.csv', table, delimiter=',', fmt='%s')

    # use pandas to handle the locale issue, and transpose the table
    df = pd.read_csv(code+'_'+filename+'.csv', thousands=',')
    df = df.T

    # reindex to handle 2004-2014
    if df.index[1][5:7] in ['12']:
    #if df.index[1][5:7] != df.index[-2][5:7]:
        print '########################'

    fiscal_month = df.index[1][4:7]
    df2 = df.reindex([str(i)+fiscal_month for i in range(2004, 2015)] + ['Last'])
    #df2.to_csv(code+'_'+filename+'_f.csv',na_rep='0', header=False)
    df2.to_csv(code+'_'+filename+'_f.csv',na_rep='NaN', header=False)

def format_key_ratios(code):
    split_ratio_file(code, 2, 18, 'financials')
    split_ratio_file(code, 20, 30, 'margins_of_sales_pct')
    split_ratio_file(code, 31, 40, 'profitability')
    split_ratio_file(code, 42, 63, 'growth', True)
    split_ratio_file(code, 65, 71, 'cash_flow')
    split_ratio_file(code, 73, 94, 'financial_health_balance_sheet')
    split_ratio_file(code, 95, 100, 'financial_health_liquidity')
    split_ratio_file(code, 102, 111, 'efficiency_ratios')


if __name__ == '__main__':
    # change to the work directory
    pwd = os.getcwd()
    os.chdir(pwd+'/data/initial')

    # merge stock samples
    fd1 = open('eod.pkl', 'rb')
    ok_dic1, failed_dic1, empty_dic1, updated_dic1 = pickle.load(fd1)
    fd2 = open('ratios.pkl', 'rb')
    ok_dic2, failed_dic2, empty_dic2, updated_dic2 = pickle.load(fd2)
    eod_ratios = {}
    for key in updated_dic1.keys():
        if key in updated_dic2.keys():
            eod_ratios[key] = updated_dic1[key]
    fd = open('eod_ratios.pkl', 'wb')
    pickle.dump(eod_ratios, fd)
    print 'EOD and RATIOS stock list are merged.\n'
    time.sleep(1)

    ###############
    # clean key_ratios
    ###############
    print 'Cleaning key ratios...'
    # load code and name: updated_dic
    fd = open('eod_ratios.pkl', 'r')
    updated_dic = pickle.load(fd)

    # format
    for code in updated_dic.keys():
        print code
        format_key_ratios(code)
    print 'Finish cleaning key ratios!\n'
